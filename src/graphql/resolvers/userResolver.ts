import axios from "axios";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { FastifyContext } from "fastify";
import {
  genAccessToken,
  genRefreshToken,
  getAuthPayload,
} from "../../helpers/authHelper";
import {
  BASE_URI,
  JWT_SECRET,
  REFRESH_TOKEN_NOT_MATCHED_ERROR,
  UNAUTHORIZED_ACCESS_ERROR,
  UPDATE_REFRESH_TOKEN_FAIL_ERROR,
  USER_NOT_FOUND_ERROR,
  VERIFY_TOKEN_ERROR,
  WRONG_PASSWORD_ERROR,
  WRONG_USERNAME_ERROR,
  X_HASURA_ADMIN_SECRET,
} from "../../lib/constants";
import { LoginInput } from "../../types/auth";

const headers = {
  "Content-Type": "application/json",
  "x-hasura-admin-secret": X_HASURA_ADMIN_SECRET,
};

const userResolver = {
  Mutation: {
    login: async (
      parent: any,
      args: LoginInput,
      context: FastifyContext,
      info: any
    ) => {
      const { email, password } = args;
      const query = `#graphql
        query QueryUser {
            users_aggregate(where: {email: {_eq: "${email}"}}) {
                nodes {
                    email
                    password
                    role
                    refresh_token
                }
            }
        }
      `;
      const resFindUser = await axios.post(BASE_URI, { query }, { headers });
      const findUser = resFindUser.data.data.users_aggregate.nodes[0];

      // Check if user exists
      if (!findUser) {
        throw new Error(WRONG_USERNAME_ERROR);
      }

      // Compare password
      if (!(await bcrypt.compare(password, findUser.password))) {
        throw new Error(WRONG_PASSWORD_ERROR);
      }
      const refreshToken = genRefreshToken(
        getAuthPayload(email, findUser.role)
      );

      // Update refresh token
      const mutation = `#graphql
            mutation updateRefreshToken {
                update_users_by_pk(pk_columns: {email: "${email}"}, _set: {refresh_token: "${refreshToken}"}) {
                    email
                    role
                    refresh_token
                }
            }
        `;
      const resUpdateRefreshToken = await axios.post(
        BASE_URI,
        { query: mutation },
        { headers }
      );
      const updateUser = resUpdateRefreshToken.data.data.update_users_by_pk;

      if (!updateUser) {
        throw new Error(UPDATE_REFRESH_TOKEN_FAIL_ERROR);
      }

      return {
        accessToken: genAccessToken(getAuthPayload(email, updateUser.role)),
      };
    },
    refreshToken: async (
      parent: any,
      args: { email: string },
      context: FastifyContext,
      info: any
    ) => {
      const { email } = args;
      const query = `#graphql
        query QueryUser {
          users_aggregate(where: {email: {_eq: "${email}"}}) {
            nodes {
              email
              password
              role
              refresh_token
            }
          }
        }
      `;

      const resFindUser = await axios.post(BASE_URI, { query }, { headers });
      const user = resFindUser.data.data.users_aggregate.nodes[0];
      if (!user) {
        throw new Error(USER_NOT_FOUND_ERROR);
      }

      const decoded = jwt.verify(
        user.refresh_token,
        JWT_SECRET,
        (err: any, decoded: any) => {
          if (err) {
            throw new Error(VERIFY_TOKEN_ERROR);
          }
          if (user.email !== decoded.email) {
            throw new Error(REFRESH_TOKEN_NOT_MATCHED_ERROR);
          }
          const accessToken = genAccessToken(getAuthPayload(email, user.role));
          return { accessToken };
        }
      );

      return decoded;
    },
    logout: async (
      parent: any,
      args: { email: string },
      context: FastifyContext,
      info: any
    ) => {
      const { email } = args;
      const query = `#graphql
        query QueryUser {
          users_by_pk(email: "${email}") {
            email
            refresh_token
            role
          }
        }
      `;
      const resFindUser = await axios.post(BASE_URI, { query }, { headers });
      const user = resFindUser.data.data.users_by_pk;

      // No user found in db
      if (!user) {
        return;
      }

      const { id } = user;
      const mutation = `#graphql
        mutation updateRefreshToken {
          update_users_by_pk(pk_columns: {id: "${id}"}, _set: {refresh_token: ""}) {
            email
            refresh_token
            role
          }
        }
      `;
      const resUpdateRefreshToken = await axios.post(
        BASE_URI,
        { query: mutation },
        { headers }
      );
    },
  },
};

export default userResolver;
