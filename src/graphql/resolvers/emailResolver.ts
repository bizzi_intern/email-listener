import axios from "axios";
import { FastifyContext } from "fastify";
import GraphQLJSON from "graphql-type-json";
import { BASE_URI, UNAUTHORIZED_ACCESS_ERROR } from "../../lib/constants";

const headers = {
  "Content-Type": "application/json",
  Authorization: "",
};

const emailResolver = {
  JSON: GraphQLJSON,
  Query: {
    emails: async (
      parent: any,
      args: any,
      context: FastifyContext,
      info: any
    ) => {
      const query = `#graphql
        query MyQuery {
          emails {
            id
            from
            date
            subject
            to
            sender_name
          }
        }
      `;
      const auth = context.request.headers.authorization;
      if (!auth || auth === "") {
        throw new Error(UNAUTHORIZED_ACCESS_ERROR);
      }
      headers.Authorization = auth;

      const res = await axios.post(
        BASE_URI,
        {
          query,
        },
        {
          headers,
        }
      );
      return res.data.data?.emails;
    },
    emailsWithAttachments: async (
      parent: any,
      args: any,
      context: FastifyContext,
      info: any
    ) => {
      const query = `#graphql
        query MyQuery {
          emails {
            id
            from
            date
            subject
            to
            sender_name
            content_url
            snippet
            attachments {
              id
              filename
              email_id
              url
            }
          }
        }
      `;
      const auth = context.request.headers.authorization;
      if (!auth || auth === "") {
        throw new Error(UNAUTHORIZED_ACCESS_ERROR);
      }
      headers.Authorization = auth;

      const res = await axios.post(
        BASE_URI,
        {
          query,
        },
        {
          headers,
        }
      );
      return res.data.data?.emails;
    },
    billsByEmailId: async (
      parent: any,
      args: { emailId: string },
      context: FastifyContext,
      info: any
    ) => {
      const { emailId } = args;
      const query = `#graphql
        query MyQuery($email_id: String!) {
          bills(email_id: $email_id) {
            buyer {
              contract_id
              end_date
              name
              start_date
              payment_method
              tax_code
              address
            }
            created_at
            currency
            email {
              content_url
              date
              from
              id
              sender_name
              snippet
              subject
              to
            }
            exchange_rate
            form
            full_id
            id
            money
            name
            seller {
              address
              name
              tax_code
              tel
            }
            serial
            services {
              bill_id
              id
              money
              name
              total_money
              vat_amount
              vat_rate
            }
            total_money
            total_money_in_words
            url
            vat_amount
            vat_rate
            attachment {
              email_id
              filename
              id
              url
            }
          }
        }
      `;
      const auth = context.request.headers.authorization;
      if (!auth || auth === "") {
        throw new Error(UNAUTHORIZED_ACCESS_ERROR);
      }
      headers.Authorization = auth;

      const res = await axios.post(
        BASE_URI,
        {
          query,
          variables: {
            emailId,
          },
        },
        {
          headers,
        }
      );
      return res.data;
      // return res.data.data?.bills;
    },
  },
};

export default emailResolver;
