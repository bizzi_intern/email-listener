const emailTypeDefs = `#graphql
    type Email {
        id: String!
        to: String!
        from: String!
        sender_name: String!
        date: String!
        subject: String!
        content_url: String!
        snippet: String!
    }
    type Attachment {
        id: String!
        filename: String
        email_id: String
        url: String
    }
    type EmailWithAttachments {
        id: String!
        to: String
        from: String
        sender_name: String
        date: String
        subject: String
        content_url: String
        snippet: String
        attachments: [Attachment]
    }
    type Buyer {
        contract_id: String
        end_date: String
        name: String
        start_date: String
        payment_method: String
        tax_code: String
        address: String
    }
    type Email {
        content_url: String
        date: String
        from: String
        id: String!
        sender_name: String
        snippet: String
        subject: String
        to: String
    }
    type Seller {
        address: String
        name: String
        tax_code: String!
        tel: String
    }
    type Service {
        bill_id: String
        id: String!
        money: String
        name: String
        total_money: String
        vat_amount: String
        vat_rate: String
    }
    type Bill {
        attachment_id: String
        total_money: String
        total_money_in_words: String
        url: String
        vat_amount: String
        vat_rate: String
        serial: String
        exchange_rate: String
        form: String
        full_id: String
        id: String!
        money: String
        name: String
        created_at: String
        currency: String
        services: [Service]
        seller: Seller
        buyer: Buyer
        email: Email
        attachments: [Attachment]
    }
    extend type Query {
        emails: [Email!]!
        emailsWithAttachments: [EmailWithAttachments]
        billsByEmailId(emailId: String): [Bill]
    }
    extend type Mutation {
        hello: String!
    }
`;

export default emailTypeDefs;
