const userTypeDefs = `#graphql
    type AccessToken {
        accessToken: String!
    }
    extend type Mutation {
        login(email: String!, password: String!): AccessToken!
        refreshToken(email: String!): AccessToken!
        logout(email: String!): Boolean
    }
`;

export default userTypeDefs;
