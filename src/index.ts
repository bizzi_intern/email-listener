import _ from "lodash";
import { ApolloServer } from "@apollo/server";
import fastifyApollo, {
  fastifyApolloDrainPlugin,
} from "@as-integrations/fastify";
import "dotenv/config";

import app from "./app";
import MyContext from "./types/MyContext";
import { FastifyReply, FastifyRequest } from "fastify";

import { PORT } from "./lib/constants";
import emailTypeDefs from "./graphql/typeDefs/emailTypeDefs";
import emailResolver from "./graphql/resolvers/emailResolver";
import userResolver from "./graphql/resolvers/userResolver";
import userTypeDefs from "./graphql/typeDefs/userTypeDefs";

const myPort = PORT || 3001;

const baseTypeDefs = `
  scalar JSON
  type Query
  type Mutation
`;

const apollo = new ApolloServer<MyContext>({
  typeDefs: [baseTypeDefs, emailTypeDefs, userTypeDefs],
  resolvers: _.merge({}, emailResolver, userResolver),
  plugins: [fastifyApolloDrainPlugin(app)],
});

apollo.start().then(() => {
  app.register(fastifyApollo(apollo), {
    context: (request: FastifyRequest, reply: FastifyReply) => {
      return {
        reply,
        request,
      } as any;
    },
  });
  // app.listen({ port: myPort }, async (err: Error | null, address: string) => {
  //   if (err) {
  //     console.error(err);
  //     process.exit(1);
  //   }
  //   console.log(`Server is listening at ${address}`);
  // });
});

export { app };
export default apollo;
