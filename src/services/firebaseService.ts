// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDownloadURL, getStorage, ref, uploadBytes } from "firebase/storage";
import { decodeBase64ToBytes } from "../helpers/base64Helper";
import { FIREBASE_CONFIG } from "../lib/constants";

// Initialize Firebase
const firebase = initializeApp(FIREBASE_CONFIG);

// Initialize Cloud Storage and get a reference to the service
const storage = getStorage(firebase);

async function uploadEmailContent(emailId: string, emailContent: string) {
  const fileRef = ref(storage, `emails/${emailId}`);
  const bytes = decodeBase64ToBytes(emailContent);
  await uploadBytes(fileRef, bytes);
  return await getDownloadURL(fileRef);
}

async function uploadAttachment(attachmentId: string, attachmentData: string) {
  const bytes = decodeBase64ToBytes(attachmentData);
  const fileRef = ref(storage, `attachments/${attachmentId}`);
  await uploadBytes(fileRef, bytes);
  return await getDownloadURL(fileRef);
}

async function uploadBill(billId: string, billData: string) {
  const bytes = decodeBase64ToBytes(billData);
  const fileRef = ref(storage, `bills/${billId}`);
  await uploadBytes(fileRef, bytes);
  return await getDownloadURL(fileRef);
}

export { uploadBill, uploadEmailContent, uploadAttachment };
