import { gmail_v1 } from "googleapis";
import { EmailPull, Part, Email } from "../types/email";
import { getExtension } from "../helpers/fileHelper";
import { parseDetails, decodeAttachmentToJSON } from "../helpers/emailHelper";
import { uploadAttachment } from "./firebaseService";

const pullEmails = async (gmail: gmail_v1.Gmail, limit: number) => {
  const res = await gmail.users.messages.list({
    userId: "me",
    maxResults: limit,
  });
  return res.data.messages as EmailPull[];
};

const pullEmailsWithDetails = async (
  gmail: gmail_v1.Gmail,
  emails: EmailPull[]
) => {
  const emailsWithDetails = [];
  for (let i = 0; i < emails.length; i++) {
    const resDetails = await gmail.users.messages.get({
      id: emails[i].id,
      userId: "me",
    });
    const details = await parseDetails({ ...emails[i], ...resDetails.data });
    emailsWithDetails.push(details);
  }

  return emailsWithDetails as Array<
    Email & { parts?: Part[]; attachments?: Part[] }
  >;
};

const pullAttachments = async (
  emails: Array<Email & { parts?: Part[]; attachments?: Part[] }>
) => {
  const emailsWithAttachments = emails.map((email) => {
    const parts = email.parts?.filter(
      (part: Part) => String(part.filename) !== ""
    ) as Part[];

    if (parts?.length > 0) {
      return parts.map((item: Part) => ({
        id: item.body?.attachmentId,
        messageId: email.id,
        filename: item.filename,
      }));
    }
    return [];
  }) as any;
  const attachments = [].concat(...emailsWithAttachments) as Array<{
    id: string;
    messageId: string;
    filename: string;
  }>;

  return attachments;
};

const pullAttachmentsWithData = async (
  gmail: gmail_v1.Gmail,
  attachments: Array<{ id: string; messageId: string; filename: string }>
) => {
  const attachmentsWithData = [];

  for (let i = 0; i < attachments.length; i++) {
    let item;
    const res = await gmail.users.messages.attachments.get({
      id: attachments[i].id,
      messageId: attachments[i].messageId,
      userId: "me",
    });
    if (res.data.data) {
      const url = await uploadAttachment(
        attachments[i].id,
        res.data.data as string
      );
      if (getExtension(attachments[i].filename) === "xml") {
        const data = decodeAttachmentToJSON(res.data.data as string);
        item = { ...attachments[i], url, data };
      }
      attachmentsWithData.push(item);
    }
  }
  return attachmentsWithData;
};

export {
  pullEmails,
  pullEmailsWithDetails,
  pullAttachments,
  pullAttachmentsWithData,
};
