import fs from "fs/promises";
import path from "path";
import process from "process";
import { authenticate } from "@google-cloud/local-auth";
import { google } from "googleapis";
import { OAuth2Client } from "google-auth-library";
import { CREDENTIALS_PATH, GMAIL_SCOPE, TOKEN_PATH } from "../lib/constants";

const scopes = [GMAIL_SCOPE];
const tokenPath = path.join(process.cwd(), TOKEN_PATH);
const credentialsPath = path.join(process.cwd(), CREDENTIALS_PATH);

// Read previously authorized credentials from the save file.
async function loadSavedCredentialsIfExist() {
  try {
    const content = String(await fs.readFile(tokenPath));
    const credentials = JSON.parse(content);
    return google.auth.fromJSON(credentials);
  } catch (err) {
    return null;
  }
}

// Serializes credentials to a file compatible with GoogleAUth.fromJSON.
async function saveCredentials(client: OAuth2Client) {
  const content = String(await fs.readFile(credentialsPath));
  const keys = JSON.parse(content);
  const key = keys.installed || keys.web;
  const payload = JSON.stringify({
    type: "authorized_user",
    client_id: key.client_id,
    client_secret: key.client_secret,
    refresh_token: client.credentials.refresh_token,
  });
  await fs.writeFile(tokenPath, payload);
}

const gmailAuth = async () => {
  let client = (await loadSavedCredentialsIfExist()) as OAuth2Client;
  if (!client) {
    client = await authenticate({
      scopes,
      keyfilePath: credentialsPath,
    });
    if (client.credentials) {
      await saveCredentials(client);
    }
  }
  const gmail = google.gmail({ version: "v1", auth: client });
  return gmail;
};

export default gmailAuth;
