import axios from "axios";
import { parseAttachments, splitAttachments } from "../helpers/emailHelper";
import { BASE_URI, X_HASURA_ADMIN_SECRET } from "../lib/constants";
import {
  Attachment,
  Bill,
  Buyer,
  Email,
  Seller,
  Service,
} from "../types/email";
import gmailAuth from "./gmailAuthService";
import {
  pullAttachments,
  pullAttachmentsWithData,
  pullEmailsWithDetails,
  pullEmails,
} from "./gmailPullService";

const headers = {
  "Content-Type": "application/json",
  "x-hasura-admin-secret": X_HASURA_ADMIN_SECRET,
};

const limit = 10;

const getEmails = async () => {
  const query = `#graphql
        query getEmails {
            emails {
              id
              from
              date
              subject
              to
              sender_name
              content_url
              snippet
            }
        }
    `;
  const response = await axios.post(
    BASE_URI,
    {
      query,
    },
    {
      headers,
    }
  );
  return response.data.data.emails;
};

const getBills = async () => {
  const query = `#graphql
    query getBills {
      bills {
        id
        serial
        form
        name
        full_id
        exchange_rate
        currency
        email_id
        seller_tax_code
        buyer_tax_code
        created_at
        url
        money
        vat_amount
        vat_rate
        total_money
        total_money_in_words
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query,
    },
    {
      headers,
    }
  );
  return response.data.data.bills;
};

const getSellers = async () => {
  const query = `#graphql
    query getSellers {
      sellers {
        tax_code
        name
        address
        tel
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query,
    },
    {
      headers,
    }
  );
  return response.data.data.sellers;
};

const getBuyers = async () => {
  const query = `#graphql
    query getBuyers {
      buyers {
        tax_code
        name
        address
        payment_method
        contract_id
        start_date 
        end_date 
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query,
    },
    {
      headers,
    }
  );
  return response.data.data.buyers;
};

const getServices = async () => {
  const query = `#graphql
    query getServices {
      services {
        id
        bill_id
        name
        money
        vat_rate
        vat_amount
        total_money
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query,
    },
    {
      headers,
    }
  );
  return response.data.data.services;
};

const getAttachments = async () => {
  const query = `#graphql
    query getAttachments {
      attachments {
        id
        filename
        url
        email_id
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query,
    },
    {
      headers,
    }
  );
  return response.data.data.attachments;
};

const insertEmail = async (email: Email) => {
  const mutation = `#graphql
    mutation insertEmail($object: emails_insert_input!) {
      insert_emails_one(object: $object) {
        id
        from
        date
        subject
        to
        sender_name
        content_url
        snippet
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { object: email },
    },
    {
      headers,
    }
  );
  return response.data.insert_emails_one;
};

const insertService = async (service: Service) => {
  const mutation = `#graphql
    mutation insertService($object: services_insert_input!) {
        insert_services_one(object: $object) {
          returning {
            id
            bill_id
            name
            money
            vat_rate
            vat_amount
            total_money
          }
        }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { object: service },
    },
    {
      headers,
    }
  );

  return response.data.data.insert_services_one;
};

const insertBuyer = async (buyer: Buyer) => {
  const mutation = `#graphql
    mutation insertBuyer($object: buyers_insert_input!) {
        insert_buyers_one(object: $object) {
          returning {
            tax_code
            name
            address
            payment_method
            contract_id
            start_date
            end_date
          }
        }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { object: buyer },
    },
    {
      headers,
    }
  );

  return response.data.data.insert_buyers_one;
};

const insertSeller = async (seller: Seller) => {
  const mutation = `#graphql
    mutation insertSeller($object: sellers_insert_input!) {
        insert_sellers_one(object: $object) {
          returning {
            tax_code
            name
            address
            tel
          }
        }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { object: seller },
    },
    {
      headers,
    }
  );

  return response.data.data.insert_sellers_one;
};

const insertBill = async (bill: Bill) => {
  const mutation = `#graphql
    mutation insertBill($object: bills_insert_input!) {
        insert_bills_one(object: $object) {
          returning {
            id
            serial
            form
            name
            full_id
            exchange_rate
            currency
            email_id
            seller_tax_code
            buyer_tax_code
            created_at
            url
            money
            vat_amount
            vat_rate
            total_money
            total_money_in_words
          }
        }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { object: bill },
    },
    {
      headers,
    }
  );

  return response.data.data.insert_bills_one;
};

const insertAttachment = async (attachment: Attachment) => {
  const mutation = `#graphql
    mutation insertAttachment($object: attachments_insert_input!) {
      insert_attachments_one(object: $object) {
        id
        filename
        url
        email_id
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { object: attachment },
    },
    {
      headers,
    }
  );
  return response.data.insert_attachments_one;
};

const insertEmails = async (emails: Email[]) => {
  const mutation = `#graphql
  mutation insertEmails($objects: [emails_insert_input!]!) {
    insert_emails(objects: $objects) {
      returning {
        id
        from
        date
        subject
        to
        sender_name
        content_url
        snippet
      }
    }
  }
`;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { objects: emails },
    },
    {
      headers,
    }
  );

  return response.data.data.insert_emails;
};

const insertServices = async (services: Service[]) => {
  const mutation = `#graphql
    mutation insertServices($objects: [services_insert_input!]!) {
      insert_services(objects: $objects) {
        returning {
          id
          bill_id
          name
          money
          vat_rate
          vat_amount
          total_money
        }
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { objects: services },
    },
    {
      headers,
    }
  );

  return response.data.data.insert_services;
};

const insertBuyers = async (buyers: Buyer[]) => {
  const mutation = `#graphql
  mutation insertBuyers($objects: [buyers_insert_input!]!) {
    insert_buyers(objects: $objects) {
      returning {
        tax_code
        name
        address
        payment_method
        contract_id
        start_date
        end_date
      }
    }
  }
`;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { objects: buyers },
    },
    {
      headers,
    }
  );
  return response.data.data.insert_buyers;
};

const insertSellers = async (sellers: Seller[]) => {
  const mutation = `#graphql
  mutation insertSellers($objects: [sellers_insert_input!]!) {
    insert_sellers(objects: $objects) {
      returning {
        tax_code
        name
        address
        tel
      }
    }
  }
`;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { objects: sellers },
    },
    {
      headers,
    }
  );
  return response.data.data.insert_sellers;
};

const insertBills = async (bills: Bill[]) => {
  const mutation = `#graphql
    mutation insertBills($objects: [bills_insert_input!]!) {
      insert_bills(objects: $objects) {
        returning {
          id
          serial
          form
          name
          full_id
          exchange_rate
          currency
          email_id
          seller_tax_code
          buyer_tax_code
          created_at
          url
          money
          vat_amount
          vat_rate
          total_money
          total_money_in_words
        }
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { objects: bills },
    },
    {
      headers,
    }
  );

  return response.data.data.insert_bills;
};

const insertAttachments = async (attachments: Attachment[]) => {
  const mutation = `#graphql
    mutation insertAttachments($objects: [attachments_insert_input!]!) {
      insert_attachments(objects: $objects) {
        returning {
          id
          filename
          url
          email_id
        }
      }
    }
  `;
  const response = await axios.post(
    BASE_URI,
    {
      query: mutation,
      variables: { objects: attachments },
    },
    {
      headers,
    }
  );

  return response.data.data.insert_attachments;
};

const loadEmailsToDb = async (fetchedEmails: Email[]) => {
  if (fetchedEmails) {
    const dbEmails = await getEmails();
    if (dbEmails.length === 0) {
      await insertEmails(fetchedEmails);
    } else {
      for (let i = 0; i < fetchedEmails.length; i++) {
        const findEmail = dbEmails.find(
          (dbEmail: { id: string }) => dbEmail.id === fetchedEmails[i].id
        );
        if (!findEmail) {
          await insertEmail(fetchedEmails[i]);
        }
      }
    }
  }
};

const loadSellersToDb = async (fetchedSellers: Seller[]) => {
  if (fetchedSellers) {
    const dbSellers = await getSellers();
    if (dbSellers.length === 0) {
      await insertSellers(fetchedSellers);
    } else {
      for (let i = 0; i < fetchedSellers.length; i++) {
        const findSeller = dbSellers.find(
          (dbSeller: { tax_code: string }) =>
            dbSeller.tax_code === fetchedSellers[i].tax_code
        );
        if (!findSeller) {
          await insertSeller(fetchedSellers[i]);
        }
      }
    }
  }
};

const loadBuyersToDb = async (fetchedBuyers: Buyer[]) => {
  if (fetchedBuyers) {
    const dbBuyers = await getBuyers();
    if (dbBuyers.length === 0) {
      await insertBuyers(fetchedBuyers);
    } else {
      for (let i = 0; i < fetchedBuyers.length; i++) {
        const findBuyer = dbBuyers.find(
          (dbBuyer: { tax_code: string }) =>
            dbBuyer.tax_code === fetchedBuyers[i].tax_code
        );
        if (!findBuyer) {
          await insertBuyer(fetchedBuyers[i]);
        }
      }
    }
  }
};

const loadBillsToDb = async (fetchedBills: Bill[]) => {
  if (fetchedBills) {
    const dbBills = await getBills();
    if (dbBills.length === 0) {
      await insertBills(fetchedBills);
    } else {
      for (let i = 0; i < fetchedBills?.length; i++) {
        const findBill = dbBills.find(
          (dbBill: { id: string }) => dbBill.id === fetchedBills[i].id
        );
        if (!findBill) {
          await insertBill(fetchedBills[i]);
        }
      }
    }
  }
};

const loadServicesToDb = async (fetchedServices: Service[]) => {
  if (fetchedServices) {
    const dbServices = await getServices();
    if (dbServices.length === 0) {
      await insertServices(fetchedServices);
    } else {
      for (let i = 0; i < fetchedServices?.length; i++) {
        const findService = dbServices.find(
          (dbService: { id: number; bill_id: string }) =>
            dbService.id === fetchedServices[i].id &&
            dbService.bill_id === fetchedServices[i].bill_id
        );
        if (!findService) {
          await insertService(fetchedServices[i]);
        }
      }
    }
  }
};

const loadAttachmentsToDb = async (fetchedAttachments: Attachment[]) => {
  if (fetchedAttachments) {
    const dbAttachments = await getAttachments();
    if (dbAttachments.length === 0) {
      await insertAttachments(fetchedAttachments);
    } else {
      for (let i = 0; i < fetchedAttachments?.length; i++) {
        const findAttachment = dbAttachments.find(
          (dbAttachment: { id: string }) =>
            dbAttachment.id === fetchedAttachments[i].id
        );
        if (!findAttachment) {
          await insertAttachment(fetchedAttachments[i]);
        }
      }
    }
  }
};

const setUpEmailServices = async (limit: number) => {
  const gmail = await gmailAuth();
  const emails = await pullEmails(gmail, limit);
  const emailsWithDetails = await pullEmailsWithDetails(gmail, emails);
  const attachments = await pullAttachments(emailsWithDetails);
  const attachmentsWithData = await pullAttachmentsWithData(gmail, attachments);

  let data;
  if (attachmentsWithData.length > 0) {
    const parsedAttachments = parseAttachments(attachmentsWithData);

    // Remove empty attachments/wrong defined bill
    const filteredAttachments = parsedAttachments.filter((item: any) => item);
    if (filteredAttachments.length > 0) {
      data = splitAttachments(filteredAttachments);
    }
  }

  const emailsToLoad = emailsWithDetails.map((email) => {
    delete email.parts;
    delete email.attachments;
    return email;
  });

  try {
    await loadEmailsToDb(emailsToLoad);
    if (data?.bill?.length > 0) {
      await loadBillsToDb(data.bill);
    }
    if (data?.attachment?.length > 0) {
      await loadAttachmentsToDb(data.attachment);
    }
    if (data?.seller?.length > 0) {
      await loadSellersToDb(data.seller);
    }
    if (data?.buyer?.length > 0) {
      await loadBuyersToDb(data.buyer);
    }
    if (data?.service?.length > 0) {
      await loadServicesToDb(data.service);
    }
  } catch (err) {
    console.error(err);
  }
};

export { setUpEmailServices };
