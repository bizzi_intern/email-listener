/***  Error Message ***/
export const USER_ALREADY_EXISTS_ERROR = "User Already Exists Error";
export const WRONG_USERNAME_ERROR =
  "Wrong Username / User Not Registered Error";
export const WRONG_PASSWORD_ERROR = "Wrong Password Error";
export const NOT_AUTHENTICATED_ERROR = "Not Authenticated Error";
export const UNAUTHORIZED_ACCESS_ERROR = "Unauthorized Access Error";
export const NO_REFRESH_TOKEN_ERROR = "No Refresh Token In Cookies Error";
export const INVALID_REFRESH_TOKEN_ERROR =
  "Refresh Token Are Not In Database Or Does Not Matched Error";
export const REFRESH_TOKEN_NOT_MATCHED_ERROR =
  "Refresh Token Not Matched Error";
export const VERIFY_TOKEN_ERROR = "Fail To Verify Token Error";
export const USER_NOT_FOUND_ERROR = "User Not Found Error";
export const UPDATE_REFRESH_TOKEN_FAIL_ERROR =
  "Update Refresh Token Fail Error";

/*** Environment variables ***/
export const {
  PORT,
  PROJECT_ID,
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI,
  REFRESH_TOKEN,
  JWT_SECRET,
  COOKIE_SECRET,
  GOOGLE_APPLICATION_CREDENTIALS,
  TOKEN_PATH,
  CREDENTIALS_PATH,
  GMAIL_SCOPE,
  DATABASE_URL,
  X_HASURA_ADMIN_SECRET,
  BASE_URI,
  API_KEY,
  FIREBASE_API_KEY,
  FIREBASE_AUTH_DOMAIN,
  FIREBASE_PROJECT_ID,
  FIREBASE_STORAGE_BUCKET,
  FIREBASE_MESSAGING_SENDER_ID,
  FIREBASE_APP_ID,
  FIREBASE_MEASUREMENT_ID,
} = process.env;

export const FIREBASE_CONFIG = {
  apiKey: FIREBASE_API_KEY,
  authDomain: FIREBASE_AUTH_DOMAIN,
  projectId: FIREBASE_PROJECT_ID,
  storageBucket: FIREBASE_STORAGE_BUCKET,
  messagingSenderId: FIREBASE_MESSAGING_SENDER_ID,
  appId: FIREBASE_APP_ID,
  measurementId: FIREBASE_MEASUREMENT_ID,
};
