export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      PORT: number;
      PROJECT_ID: string;
      CLIENT_ID: string;
      CLIENT_SECRET: string;
      REDIRECT_URI: string;
      REFRESH_TOKEN: string;
      GOOGLE_APPLICATION_CREDENTIALS: string;
      TOKEN_PATH: string;
      CREDENTIALS_PATH: string;
      JWT_SECRET: string;
      COOKIE_SECRET: string;
      GMAIL_SCOPE: string;
      DATABASE_URL: string;
      X_HASURA_ADMIN_SECRET: string;
      BASE_URI: string;
    }
  }
}
