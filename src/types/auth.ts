export type AuthPayload = {
  sub?: string;
  name?: string;
  admin?: boolean;
  iat?: number;
  "https://hasura.io/jwt/claims": {
    "x-hasura-default-role": string;
    "x-hasura-allowed-roles": string[];
    "x-hasura-user-id"?: string;
    "x-hasura-org-id"?: string;
    "x-hasura-custom"?: string;
  };
};

export type LoginInput = {
  email: string;
  password: string;
};

export type LoginOutput = {
  accessToken: string;
};
