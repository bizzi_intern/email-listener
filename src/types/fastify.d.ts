export {};

declare module "fastify" {
  interface FastifyContext {
    isAuthenticated: boolean;
    isAdmin: boolean;
    email: gmail_v1.Gmail;
    request: FastifyRequest;
    reply: FastifyReply;
  }
}
