export type Header = {
  name: string;
  value: string;
};

export type Body = {
  attachmentId?: string;
  size: number;
  data?: string;
};

export type Part = {
  partId: string;
  mimeType: string;
  filename?: string;
  headers: Array<Header>;
  body?: Body;
  parts?: Array<Part>;
};

export type EmailPull = {
  id: string;
  threadId: string;
};

export type Email = {
  id: string;
  to: string;
  from: string;
  sender_name: string;
  date: Date;
  subject: string;
  content_url: string;
  snippet: string;
};

export type Bill = {
  id: string;
  serial: string;
  form: string;
  name: string;
  created_at: Date;
  currency: string;
  exchange_rate: number;
  full_id: string;
  email_id: string;
  buyer_tax_code: string;
  seller_tax_code: string;
  url: string;
  money: number;
  vat_rate: number;
  vat_amount: number;
  total_money: number;
  total_money_in_words: string;
};

export type Buyer = {
  tax_code: string;
  name: string;
  address: string;
  payment_method: string;
  contract_id: string;
  start_date: Date;
  end_date: Date;
};

export type Seller = {
  tax_code: string;
  name: string;
  address: string;
  tel: string;
};

export type Service = {
  id: number;
  bill_id: string;
  name: string;
  money: number;
  vat_rate: number;
  vat_amount: number;
  total_money: number;
};

export type Attachment = {
  id: string;
  filename: string;
  url: string;
  email_id: string;
};
