import { BaseContext } from "@apollo/server";
import { FastifyReply, FastifyRequest } from "fastify";

interface MyContext extends BaseContext {
  reply: FastifyReply;
  request: FastifyRequest;
}

export default MyContext;
