import jwt from "jsonwebtoken";
import { JWT_SECRET } from "../lib/constants";

const getAuthPayload = (email: string, role: string) => {
  return {
    email,
    role,
    "https://hasura.io/jwt/claims": {
      "x-hasura-default-role": role,
      "x-hasura-allowed-roles": ["user", "admin"],
      "x-hasura-user-id": email,
    },
  };
};

const genAccessToken = (payload: any) => {
  return jwt.sign(payload, JWT_SECRET, {
    expiresIn: "1d",
  });
};

const genRefreshToken = (payload: any) => {
  return jwt.sign(payload, JWT_SECRET, {
    expiresIn: "3d",
  });
};

export { genAccessToken, genRefreshToken, getAuthPayload };
