import convert from "xml-js";
import { uploadEmailContent } from "../services/firebaseService";
import { Body, Header, Part } from "../types/email";
import { decodeBase64ToString } from "./base64Helper";

const defaultUser = "user1@gmail.com";

function getAddress(from: string) {
  if (!from.includes("<") || !from.includes(">")) {
    return from;
  }
  let regExpExecArray = /<(.*)>/.exec(from);
  if (regExpExecArray) {
    let address = regExpExecArray[1];
    return address.replace(/<.*?>/g, "");
  }
  return "";
}

function getSenderName(from: string) {
  let senderName = from.replace(/<.*?>/g, "").slice(0, -1);
  return senderName.replace(/"/g, "");
}

async function parseDetails(email: any) {
  const id: string = email.id;
  const snippet: string = email.snippet;
  const from: string = email.payload.headers.find(
    (header: Header) => header.name === "From"
  )?.value;
  const to: string = email.payload.headers.find(
    (header: Header) => header.name === "To"
  )?.value;
  const date = email.payload.headers.find(
    (header: Header) => header.name === "Date"
  )?.value;
  const subject: string = email.payload.headers.find(
    (header: Header) => header.name === "Subject"
  )?.value;
  const fullContentType: string = email.payload.headers.find(
    (header: Header) =>
      header.name === "Content-Type" || header.name === "Content-type"
  )?.value;
  const parts: Part[] = email.payload.parts;
  const attachments = parts?.filter(
    (part: Part) => String(part.filename) !== ""
  );

  let toAddress: string;
  if (to === "") {
    toAddress = defaultUser;
  } else {
    toAddress = getAddress(to) as string;
  }
  const contentType = fullContentType.substring(
    0,
    fullContentType.indexOf(";")
  );
  let content;
  if (contentType === "text/html") {
    content = getEmailContentFromBody(email.payload.body);
  } else {
    content = getEmailContentFromParts(contentType, parts);
  }
  let url;
  try {
    url = await uploadEmailContent(id, content);
  } catch (err) {
    console.error(err);
  }

  return {
    id,
    to: toAddress,
    from: getAddress(from) as string,
    sender_name: getSenderName(from) as string,
    date: new Date(date),
    subject,
    content_url: url,
    snippet,
    parts,
    attachments,
  };
}

function getEmailContentFromBody(body: Body) {
  if (body.data) {
    return body.data;
  } else {
    return "";
  }
}

function getEmailContentFromParts(contentType: string, parts: Part[]) {
  if (!parts || parts.length == 0) {
    return "";
  }

  let content;
  // multipart/alternative content type
  if (contentType === "multipart/alternative") {
    content = parts.find((part) => Number(part.partId) === 1)?.body?.data;
  }
  // multipart/mixed content type
  else {
    const contentPart = parts.find((part) => Number(part.partId) === 0);
    if (contentPart?.parts) {
      content = contentPart.parts[1].body?.data;
    }
  }
  if (content) {
    return content;
  }
  return "";
}

function parseAttachment(attachment: any) {
  if (!attachment) {
    return null;
  }
  const HDon = attachment.data.elements[0].elements;
  const TTChung = HDon[0].elements[0].elements;
  if (!TTChung) {
    return null;
  }
  const MSHDon = TTChung.find(
    (item: { name: string }) => item.name === "MSHDon"
  ).elements[0].text;
  const KHHDon = TTChung.find(
    (item: { name: string }) => item.name === "KHHDon"
  ).elements[0].text;
  const SHDon = TTChung.find((item: { name: string }) => item.name === "SHDon")
    .elements[0].text;
  const THDon = TTChung.find((item: { name: string }) => item.name === "THDon")
    .elements[0].text;
  const TDLap = new Date(
    TTChung.find(
      (item: { name: string }) => item.name === "TDLap"
    ).elements[0].text
  );
  const DVTTe = TTChung.find((item: { name: string }) => item.name === "DVTTe")
    .elements[0].text;
  const TGia = Number(
    TTChung.find((item: { name: string }) => item.name === "TGia").elements[0]
      .text
  );
  const IDHDon = TTChung.find(
    (item: { name: string }) => item.name === "IDHDon"
  ).elements[0].text;

  if (
    !MSHDon ||
    !KHHDon ||
    !SHDon ||
    !THDon ||
    !TDLap ||
    !DVTTe ||
    !TGia ||
    !IDHDon
  ) {
    return null;
  }

  const NDHDon = HDon[0].elements[1].elements;
  const NBan = NDHDon[0].elements;
  if (!NBan) {
    return null;
  }
  const NBTen = NBan.find((item: { name: string }) => item.name === "NBTen")
    .elements[0].text;
  const NBMST = NBan.find((item: { name: string }) => item.name === "NBMST")
    .elements[0].text;
  const NBDChi = NBan.find((item: { name: string }) => item.name === "NBDChi")
    .elements[0].text;
  const NBDThoai = NBan.find(
    (item: { name: string }) => item.name === "NBDThoai"
  ).elements[0].text;

  if (!NBTen || !NBMST || !NBDChi || !NBDThoai) {
    return null;
  }

  const NMua = NDHDon[1].elements;
  if (!NMua) {
    return null;
  }
  const Ten = NMua.find((item: { name: string }) => item.name === "Ten")
    .elements[0].text;
  const MST = NMua.find((item: { name: string }) => item.name === "MST")
    .elements[0].text;
  const DChi = NMua.find((item: { name: string }) => item.name === "DChi")
    .elements[0].text;
  const HTTToan = NMua.find((item: { name: string }) => item.name === "HTTToan")
    .elements[0].text;
  const SHDong = NMua.find((item: { name: string }) => item.name === "SHDong")
    .elements[0].text;
  const TNgay = NMua.find((item: { name: string }) => item.name === "TNgay")
    .elements[0].text;
  const DNgay = NMua.find((item: { name: string }) => item.name === "DNgay")
    .elements[0].text;
  if (!Ten || !MST || !DChi || !HTTToan || !SHDong || !TNgay || !DNgay) {
    return null;
  }

  const HHDVu = NDHDon[2].elements[0].elements;
  if (!HHDVu) {
    return null;
  }
  const STT = HHDVu.find((item: { name: string }) => item.name === "STT")
    .elements[0].text;
  const THHDVu = HHDVu.find((item: { name: string }) => item.name === "THHDVu")
    .elements[0].text;
  const TTien = HHDVu.find((item: { name: string }) => item.name === "TTien")
    .elements[0].text;
  const TSuat = HHDVu.find((item: { name: string }) => item.name === "TSuat")
    .elements[0].text;
  const TGTGTang = HHDVu.find(
    (item: { name: string }) => item.name === "TGTGTang"
  ).elements[0].text;
  const TCong = HHDVu.find((item: { name: string }) => item.name === "TCong")
    .elements[0].text;
  if (!STT || !THHDVu || !TTien || !TSuat || !TGTGTang || !TCong) {
    return null;
  }

  const TToan = NDHDon[3].elements;
  if (!TToan) {
    return null;
  }
  const TgTCThue = TToan.find(
    (item: { name: string }) => item.name === "TgTCThue"
  ).elements[0].text;
  const TSGTGTang = TToan.find(
    (item: { name: string }) => item.name === "TSGTGTang"
  ).elements[0].text;
  const TgTThue = TToan.find(
    (item: { name: string }) => item.name === "TgTThue"
  ).elements[0].text;
  const TgTTTBSo = TToan.find(
    (item: { name: string }) => item.name === "TgTTTBSo"
  ).elements[0].text;
  const TgTTTBChu = TToan.find(
    (item: { name: string }) => item.name === "TgTTTBChu"
  ).elements[0].text;

  const bill = {
    id: SHDon,
    serial: KHHDon,
    form: MSHDon,
    name: THDon,
    created_at: TDLap,
    currency: DVTTe,
    exchange_rate: Number(TGia),
    full_id: IDHDon,
    email_id: attachment.messageId,
    buyer_tax_code: MST,
    seller_tax_code: NBMST,
    money: Number(TgTCThue.replace(/\./g, "")),
    vat_rate: Number(TSGTGTang.replace(/\./g, "")),
    vat_amount: Number(TgTThue.replace(/\./g, "")),
    total_money: Number(TgTTTBSo.replace(/\./g, "")),
    total_money_in_words: TgTTTBChu,
    attachment_id: attachment.id,
  };
  const seller = {
    tax_code: NBMST,
    name: NBTen,
    address: NBDChi,
    tel: NBDThoai,
  };
  const buyer = {
    tax_code: MST,
    name: Ten,
    address: DChi,
    payment_method: HTTToan,
    contract_id: SHDong,
    start_date: TNgay,
    end_date: DNgay,
  };
  const service = {
    id: Number(STT),
    bill_id: SHDon,
    name: THHDVu,
    money: Number(TTien.replace(/\./g, "")),
    vat_rate: Number(TSuat),
    vat_amount: Number(TGTGTang.replace(/\./g, "")),
    total_money: Number(TCong.replace(/\./g, "")),
  };
  const attachmentDb = {
    id: attachment.id,
    filename: attachment.filename,
    email_id: attachment.messageId,
    url: attachment.url,
  };

  return { bill, seller, buyer, service, attachment: attachmentDb };
}

function parseAttachments(attachments: any) {
  return attachments.map((item: any) => parseAttachment(item));
}

function splitAttachments(attachments: any) {
  const result = attachments.reduce((r: any, o: any) => {
    Object.entries(o).forEach(([k, v]) => {
      r[k] = r[k] || [];
      if (!r[k].includes(v)) r[k].push(v);
    });
    return r;
  }, Object.create(null)) as any;
  return result;
}

function decodeAttachmentToJSON(encodedData: string) {
  const options = { ignoreComment: true, alwaysChildren: true };
  const xml = decodeBase64ToString(encodedData);
  const result = convert.xml2js(xml, options);
  return result;
}

export {
  getAddress,
  getSenderName,
  getEmailContentFromBody,
  getEmailContentFromParts,
  parseDetails,
  parseAttachment,
  parseAttachments,
  decodeAttachmentToJSON,
  splitAttachments,
};
