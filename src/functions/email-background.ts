import {
  HandlerEvent,
  HandlerContext,
  Config,
  BackgroundHandler,
} from "@netlify/functions";
import { setUpEmailServices } from "../services/emailService";

export const handler: BackgroundHandler = async (
  event: HandlerEvent,
  context: HandlerContext
) => {
  await setUpEmailServices(10);
};

export const config: Config = {
  schedule: "@hourly",
};
