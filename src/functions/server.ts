import awsLambdaFastify from "@fastify/aws-lambda";
import { Handler, HandlerEvent, HandlerContext } from "@netlify/functions";
import { FastifyInstance } from "fastify";
import { app } from "../index";

const proxy = awsLambdaFastify(app as FastifyInstance);

export const handler: Handler = async (
  event: HandlerEvent,
  context: HandlerContext
) => {
  const res = await proxy(event, context);
  return res;
};
